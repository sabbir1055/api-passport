<?php

use Illuminate\Http\Request;

Route::get('/products','ProductController@index');
Route::group(['prefix'=>'products'],function(){
  Route::apiResource('/{product}/reviews','ReviewController');
});
