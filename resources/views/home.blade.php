@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(functioin(){
    $.ajax({
        type: "GET",
        url: "https://app.icontact.com/icp/a/",
        contentType: "application/json",
        beforeSend: function(jqXHR, settings){
                jqXHR.setRequestHeader("Accept", "application/json");
                jqXHR.setRequestHeader("Api-Version", iContact_API_version);
                jqXHR.setRequestHeader("Api-AppId", iContact_appID);
                jqXHR.setRequestHeader("Api-Username", iContact_username);
                jqXHR.setRequestHeader("API-Password", iContact_appPassword);}
      });
  });
</script>
@endsection
